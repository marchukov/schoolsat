#include <SimpleCLI.h>

static void initCmds()
{
  Command setFreqCmd;
  Command setPwrCmd;
  Command csvCmd;
  Command b64Cmd;
  Command rawCmd;
  Command rssiCmd;
  Command snrCmd;
  Command crcCmd;
  Command defCmd;
  Command pcktCmd;
  Command sramCmd;
  Command EepromCmd;
  Command rstCmd;
  Command txCmd;
  Command helpCmd;

  //set/get frequency
  setFreqCmd = cli.addSingleArgCmd("f", setFreqCommand);

  //set/get power
  setPwrCmd = cli.addSingleArgCmd("p", setPwrCommand);

  //set CSV output
  csvCmd = cli.addCmd("mcsv", csvCommand);

  //set Base64 output
  b64Cmd = cli.addCmd("mb64", b64Command);

  //set RAW output
  rawCmd = cli.addCmd("mraw", rawCommand);

  //set RSSI output
  rssiCmd = cli.addCmd("rssi", rssiCommand);

  //set SNR output
  snrCmd = cli.addCmd("snr", snrCommand);

  //set CRC output
  crcCmd = cli.addCmd("crc", crcCommand);

  //set default setup
  defCmd = cli.addCmd("d/efault", defaultCommand);

  //set packet structure
  pcktCmd = cli.addSingleArgCmd("packet", packetCommand);

  //get free SRAM
  sramCmd = cli.addCmd("sram", sramCommand);

  //save/get params to/from EEPROM
  EepromCmd = cli.addSingleArgCmd("e/eprom", eepromCommand);

  //restart transciever
  rstCmd = cli.addCmd("restart", restartCommand);

  //transmit packet
  txCmd = cli.addSingleArgCmd("tx", txCommand);

  //show help
  helpCmd = cli.addSingleArgCmd("h/elp", helpCommand);

  cli.setOnError(errorCallback);
}

void setFreqCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  if (argVal.length() > 0)
  {

#ifdef LIMIT
    if (argVal.toInt() < 863000000 || argVal.toInt() > 868600000)
    {
      Serial.println(F("Frequency not within 863-868.6MHz"));
      return;
    }
#endif
    l_freq = argVal.toInt();
    LoRa.setFrequency(l_freq);

    Serial.print(F("Fequency set to "));
    Serial.println(l_freq);
  }
  else
  {
    Serial.println(l_freq);
  }
}

void setPwrCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  if (argVal.length() > 0)
  {
    if (argVal.toInt() < 3 || argVal.toInt() > 23)
    {
      Serial.println(F("Power output not within 3-23dBm"));
    }
    else
    {
      i_pOut = argVal.toInt();
      LoRa.setTxPower(i_pOut);

      Serial.print(F("Power output set to "));
      Serial.println(i_pOut);
    }
  }
  else
  {
    Serial.println(i_pOut);
  }
}

void getPwrCommand(cmd *c)
{
  Serial.println(i_pOut);
}

void csvCommand(cmd *c)
{
  mode  = m_csv;
  Serial.println(F("Enabled CSV output mode"));
}

void b64Command(cmd *c)
{
  mode = m_b64;
  Serial.println(F("Enabled Base64 output mode"));
}

void rawCommand(cmd *c)
{
  mode = m_raw;
  Serial.println(F("Enabled RAW output mode"));
}

void rssiCommand(cmd *c)
{
  if (!b_rssi)
  {
    b_rssi = true;
    Serial.println(F("Enabled RSSI output"));
  }
  else
  {
    b_rssi = false;
    Serial.println(F("Disabled RSSI output"));
  }
}
void snrCommand(cmd * c)
{
  if (!b_snr)
  {
    b_snr = true;
    Serial.println(F("Enabled SNR output"));
  }
  else
  {
    b_snr = false;
    Serial.println(F("Disabled SNR output"));
  }
}

void crcCommand(cmd *c)
{
  if (!b_crc)
  {
    b_crc = true;
    Serial.println(F("Enabled CRC check"));
  }
  else
  {
    b_crc = false;
    Serial.println(F("Disabled CRC check"));
  }
}

void defaultCommand(cmd *c)
{
  mode = m_b64;
  l_freq = 867000000;
  i_pOut = 14;
  b_rssi = false;
  b_snr = false;
  b_crc = true;
  LoRa.setFrequency(l_freq);
  LoRa.setTxPower(i_pOut);
  Serial.println(F("Set to default settings"));
  Serial.println(F("\tEnabled Base64 output"));
  Serial.print(F("\tFrequency set to 867MHz"));
  Serial.println(F("\tPower output set to 13dBm"));
  Serial.println(F("\tDisabled RSSI output"));
  Serial.println(F("\tDisabled SNR output"));
  Serial.println(F("\tEnabled CRC check"));
}

void packetCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  argVal.toLowerCase();
  if (argVal.length() > 0)
  {
    byte i_expByteLen = 0;
    argVal += ',';
    String item;
    for (byte i = 0; i < argVal.length(); i++)
    {
      if (argVal[i] != ',')
      {
        item += argVal[i];
      }
      else
      {
        if (item == "f" ) i_expByteLen += 4;
        else if (item == "i8" || item == "u8") i_expByteLen += 1;
        else if (item == "i16" || item == "u16") i_expByteLen += 2;
        else if (item == "i32" || item == "u32") i_expByteLen += 4;

        else
        {
          Serial.println("Unnknow type of variable, please check your structure!");
          return;
        }
        item = "";
      }
    }
    sStructure = argVal;
    i_byteLen = i_expByteLen;
    Serial.print(F("Packet structure set to "));
    Serial.println(sStructure);
    Serial.print(F("Expected packet length: "));
    Serial.println(i_byteLen);
  }
  else
  {
    Serial.println(sStructure);
  }
}

void sramCommand(cmd *c)
{
  Serial.print(F("Free SRAM space is "));
  Serial.print(freeMemory());
  Serial.println(F(" bytes"));
}

void eepromCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  if (argVal.length() == 0)
    readEEPROM(0);
  else if (argVal.equals("use"))
    readEEPROM(1);
  else if (argVal.equals("save"))
    saveEEPROM();
}

void restartCommand(cmd *c)
{
  Serial.println(F("Resetarting..."));
  delay(100);
  resetFunc();
}

void txCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  if (argVal.equals("ping"))
  {
    ul_pingMillis = millis();
    b_pingCount = true;
  }
  if (argVal.length() > 0)
  {
    LoRa.beginPacket();                               // start packet
    LoRa.write((uint8_t *)argVal.c_str(), argVal.length()); // send msg
    LoRa.endPacket();                                 // finish packet and send it
    LoRa.receive();
  }
  else
  {
    Serial.println("Missing command to send!");
  }
}

void helpCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();
  if (argVal.length() == 0)
  {
    Serial.println(F("Commands:"));
    Serial.println(F("\tmcsv -Set CSV output"));
    Serial.println(F("\tmb64 -Set Base64 output"));
    Serial.println(F("\tmraw -Set RAW output"));
    Serial.println(F("\tf <Hz> -Set frequency, \n\tf -Get frequency"));
    Serial.println(F("\tp <dBm> -Set TX output 3-23dBm (14 max legal and default!!!), \n\tp -Get TX output"));
    Serial.println(F("\trssi -print RSSI (ON/OFF)"));
    Serial.println(F("\tsnr -print SNR (ON/OFF)"));
    Serial.println(F("\tcrc -CRC check (ON/OFF)"));
    Serial.println(F("\tsram -show free SRAM space"));
    Serial.println(F("\teeprom -Read EEPROM, \n\teeprom use -Use params from EEPROM, \n\teeprom save -Save params to EEPROM"));
    Serial.println(F("\trestart -Reset dongle"));
    Serial.println(F("\ttx <cmd> -Transmit command"));
    Serial.println(F("\tpacket <structure> -Enter csv packet structure \n\tpacket -Show actual CSV packet structure "));
    Serial.println(F("\tdefault -Enter default setup - 867MHz, Base64, RSSI & SNR output OFF, CRC check ON"));
    Serial.println(F("\thelp packet -Show help for packet command \n\thelp -Show this help"));
  }
  else if (argVal.equals("packet"))
  {
    Serial.println(F("Please use , as delimeter"));
    Serial.println(F("packet arguments"));
    Serial.println(F("\tf -float"));
    Serial.println(F("\ti8 -int8_t"));
    Serial.println(F("\tu8 -uint_8t"));
    Serial.println(F("\ti16 -int16_t"));
    Serial.println(F("\tu16 -uint16_t"));
    Serial.println(F("\ti32 -int32_t"));
    Serial.println(F("\tu32 -uint32_t"));

  }
}

void errorCallback(cmd_error *e) {
  CommandError cmdError(e); // Create wrapper object

  Serial.print("ERROR: ");
  Serial.println(cmdError.toString());

  if (cmdError.hasCommand()) {
    Serial.print("Did you mean \"");
    Serial.print(cmdError.getCommand().toString());
    Serial.println("\"?");
  }
}
