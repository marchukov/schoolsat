bool checkCRC()
{
  uint16_t i_crcReceived = ((uint16_t)cPacket[i_dataLength - 2]) & 0x00FF | ((uint16_t)(cPacket[i_dataLength - 1] << 8));
  uint16_t i_crcCalculated = crc16(cPacket, i_dataLength - 2);

  if (i_crcReceived == i_crcCalculated)
  {
    Serial.println(F("CRC OK"));
    return (true);
  }
  else
  {
    Serial.println(F("CRC BAD"));
    return (false);
  }
}

void printCSV()
{
  if (i_byteLen != i_dataLength)
  {
    Serial.println(F("Wrong packet length"));
    return;
  }
  else
  {
    uint8_t i_pos = 0;
    int32_t value = 0;
    String in_item;
    for (byte i = 0; i < sStructure.length(); i++)
    {
      if (sStructure[i] != ',')
      {
        in_item += sStructure[i];
      }
      else
      {
        if (in_item == "f")
        {
          value = (((((uint32_t)cPacket[i_pos] & 0x000000FF) | ((uint32_t)cPacket[i_pos + 1] << 8) & 0x0000FFFF) | ((uint32_t)cPacket[i_pos + 2] << 16) & 0xFFFFFF) | ((uint32_t)cPacket[i_pos + 3] << 24));
          ;
          float f_value = *(float *)&value;
          Serial.print((float)f_value);
          i_pos += 4;
        }
        else if (in_item == "i8")
        {
          value = (int8_t)cPacket[i_pos];
          Serial.print((int8_t)value);
          i_pos++;
        }
        else if (in_item == "u8")
        {
          value = (uint8_t)cPacket[i_pos];
          Serial.print((uint8_t)value);
          i_pos++;
        }
        else if (in_item == "i16")
        {
          value = ((int16_t)cPacket[i_pos]) & 0x00FF | ((int16_t)(cPacket[i_pos + 1] << 8));
          Serial.print((int16_t)value);
          i_pos += 2;
        }
        else if (in_item == "u16")
        {
          value = ((uint16_t)cPacket[i_pos]) & 0x00FF | ((uint16_t)(cPacket[i_pos + 1] << 8));
          Serial.print((uint16_t)value);
          i_pos += 2;
        }
        else if (in_item == "i32")
        {
          value = (((((int32_t)cPacket[i_pos] & 0x000000FF) | ((int32_t)cPacket[i_pos + 1] << 8) & 0x0000FFFF) | ((int32_t)cPacket[i_pos + 2] << 16) & 0x00FFFFFF) | ((int32_t)cPacket[i_pos + 3] << 24));
          Serial.print((int32_t)value);
          i_pos += 4;
        }
        else if (in_item == "u32")
        {
          value = (((((uint32_t)cPacket[i_pos] & 0x000000FF) | ((uint32_t)cPacket[i_pos + 1] << 8) & 0x0000FFFF) | ((uint32_t)cPacket[i_pos + 2] << 16) & 0xFFFFFF) | ((uint32_t)cPacket[i_pos + 3] << 24));
          Serial.print((uint32_t)value);
          i_pos += 4;
        }
        if (i_pos != i_dataLength)
          Serial.print(F(","));
        in_item = "";
      }
    }
    Serial.println(F(""));
  }
}

void printBase64()
{
  unsigned char c_encoded[((4 * i_dataLength / 3) + 3) & ~3];
  encode_base64((unsigned char *)cPacket, i_dataLength, c_encoded);
  Serial.println((char *)c_encoded);
}

void printRaw()
{
  Serial.println((char *)cPacket);
}

void printOutput()
{
  if (b_rssi)
  {
    Serial.print(F("RSSI "));
    Serial.println(LoRa.packetRssi());
  }
  if (b_snr)
  {
    Serial.print(F("SNR "));
    Serial.println(LoRa.packetSnr());
  }

  switch (mode)
  {
    case m_csv:
      printCSV();
      break;
    case m_b64:
      printBase64();
      break;
    case m_raw:
      printRaw();
      break;
  }
}

void processOutput()
{
  if (b_crc)
  {
    if (checkCRC())
      printOutput();
  }
  else
    printOutput();
}

void recievePacket(int packetSize)
{
  unsigned long ul_packetMillis = millis();
  // try to parse packet
  for (int i = 0; i < packetSize; i++)
  {
    cPacket = (char *)realloc(cPacket, i + 1);
    cPacket[i] = (char)LoRa.read();
  }
  i_dataLength = packetSize;

  if (packetSize == 4 && cPacket[0] == 'p' && cPacket[1] == 'o' && cPacket[2] == 'n' && cPacket[3] == 'g')
  {
    b_pingCount = false;
    Serial.print("Reply in ");
    Serial.print(ul_packetMillis-ul_pingMillis);
    Serial.println(" ms");
    free(cPacket);
    cPacket = NULL;
    return;
  }
  //print packet
  processOutput();
  free(cPacket);
  cPacket = NULL;
  Serial.println(F("======"));

}
