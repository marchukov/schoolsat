#include<SimpleCLI.h>


void pingRadioCommand(cmd *c);
void trxDelayCommand(cmd *c);
void epsCommand(cmd *c);
void obcCommand(cmd *c);
void gpsCommand(cmd *c);

void pingRadioCommandSerial(cmd *c);
void trxDelayCommandSerial(cmd *c);
void epsCommandSerial(cmd *c);
void obcCommandSerial(cmd *c);
void gpsCommandSerial(cmd *c);

uint8_t radioReplyBuffer[256];
void cliRadioReply(String reply)
{
  memset(radioReplyBuffer, 0, 256);
  memcpy(radioReplyBuffer, reply.c_str(), reply.length());
  unsigned short crcReply = crc16(reply.c_str(), reply.length());
  memcpy(radioReplyBuffer + reply.length(), &crcReply, 2);
  send_LoRa(radioReplyBuffer, reply.length() + 2);
}

static void initCmds()
{
  //radio ping
  radioCli.addCmd("p/ing, test", pingRadioCommand);
  serialCli.addCmd("p/ing, test", pingRadioCommandSerial);

  //set trx delay
  radioCli.addBoundlessCmd("trx", trxCommand);
  serialCli.addBoundlessCmd("trx", trxCommandSerial);

  //eps commands
  radioCli.addBoundlessCmd("eps", epsCommand);
  serialCli.addBoundlessCmd("eps", epsCommandSerial);
  
  //obc commands
  radioCli.addBoundlessCmd("obc", obcCommand);
  serialCli.addBoundlessCmd("obc", obcCommandSerial);

  //gps commands
  radioCli.addBoundlessCmd("gps", gpsCommand);
  serialCli.addBoundlessCmd("gps", gpsCommandSerial);
}

void pingRadioCommandSerial(cmd *c)
{
  Serial.println("pong");
}

void pingRadioCommand(cmd *c)
{
  send_LoRa("pong", 4);
}

void trxCommandSerial(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    // TODO what here?
  }
  else if (argVal.equals("lor") || argVal.equals("lora"))
  {
    Argument arg2 = cmd.getArgument(1);
    String arg2s = arg2.getValue();
    if (arg2s.equals("on") || arg2s.equals("1"))
    {
      LoRa_beacon_on = true;
      Serial.println("Lora beacon ON");
      }
    else if (arg2s.equals("off") || arg2s.equals("0"))
    {
      LoRa_beacon_on = false;
      Serial.println("Lora beacon OFF");
    }
  }

  else if (argVal.equals(F("dly")))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      unsigned int newInterval = cmd.getArg(1).getValue().toInt();
      if (newInterval < 5)
      {
        newInterval = 5;
      }
            
      LoRa_interval = newInterval * 1000;
      Serial.print("Lora interval ");
      Serial.println(newInterval);
    }
  }
}

void trxCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    // TODO what here?
  }
  else if (argVal.equals("lor") || argVal.equals("lora"))
  {
    Argument arg2 = cmd.getArgument(1);
    String arg2s = arg2.getValue();
    if (arg2s.equals("on") || arg2s.equals("1"))
    {
      LoRa_beacon_on = true;
    }
    else if (arg2s.equals("off") || arg2s.equals("0"))
    {
      LoRa_beacon_on = false;
    }
  }
  else if (argVal.equals(F("dly")))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      unsigned int newInterval = cmd.getArg(1).getValue().toInt();
      if (newInterval < 5)
      {
        newInterval = 5;
      }
            
      LoRa_interval = newInterval * 1000;
      
      String ddd = String(newInterval, DEC);
      String reply = String("dly " + ddd);
      Serial.print(">> ");
      Serial.println(reply);
      cliRadioReply(reply);
    }
  }
}

void epsCommand(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    batt_adc = analogRead(A0);
    float voltage = batt_adc * (5.0 / 1023.0);
    String bbb = String(voltage * 1000, 1);
    String reply = String("BV " + bbb);
    Serial.print(">> ");
    Serial.println(reply);
    cliRadioReply(reply);
  }
  
  else if (argVal.equals("set"))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      String arg_on = cmd.getArg(1).getValue();

      if (arg_on.equals("on") || arg_on.equals("1"))
      {
        digitalWrite(GREEN_LED, HIGH);
      }
      else if (arg_on.equals("off") || arg_on.equals("0"))
      {
        digitalWrite(GREEN_LED, LOW);
      }
    }
  }
}

void epsCommandSerial(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    batt_adc = analogRead(A0);
    float voltage = batt_adc * (5.0 / 1023.0);
    Serial.print("Batt volatge is: ");
    Serial.println(voltage);

  }
  else if (argVal.equals("set"))
  {
    int argc = cmd.countArgs();
    if (argc == 2)
    {
      String arg_on = cmd.getArg(1).getValue();

      if (arg_on.equals("on") || arg_on.equals("1"))
      {
        digitalWrite(GREEN_LED, HIGH);
      }
      else if (arg_on.equals("off") || arg_on.equals("0"))
      {
        digitalWrite(GREEN_LED, LOW);
      }
    }
  }
}

void obcCommand(cmd *c) // TODO Repair for radio
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    String reply = "OBC,";
    reply = String(reply + String((long)(millis() / 1000), DEC));
    reply = String(reply + ",");
    reply = String(reply + String(gx, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(gy, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(gz, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(mx, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(my, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(mz, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(pressure, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(temp, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(ntc_temp, DEC));
    Serial.print(">> ");
    Serial.println(reply);
    cliRadioReply(reply);
  }
}

void obcCommandSerial(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    Serial.print((long)(millis() / 1000));
    Serial.print(",");
    Serial.print(gx);
    Serial.print(",");
    Serial.print(gy);
    Serial.print(",");
    Serial.print(gz);
    Serial.print(",");
    Serial.print(ax);
    Serial.print(",");
    Serial.print(ay);
    Serial.print(",");
    Serial.print(az);
    Serial.print(",");
    Serial.print(mx);
    Serial.print(",");
    Serial.print(my);
    Serial.print(",");
    Serial.print(mz);
    Serial.print(",");
    Serial.print(pressure);
    Serial.print(",");
    Serial.print(temp);
    Serial.print(",");
    Serial.println(ntc_temp);
  }
}

void gpsCommand(cmd *c) // TODO Repair for radio
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    String reply = "GPS,";
    reply = String(reply + String(lat, 3));
    reply = String(reply + ",");
    reply = String(reply + String(lon, 3));
    reply = String(reply + ",");
    reply = String(reply + String(alt, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(sats, DEC));
    reply = String(reply + ",");
    reply = String(reply + String(fix, DEC));
    Serial.print(">> ");
    Serial.println(reply);
    cliRadioReply(reply);
  }
}

void gpsCommandSerial(cmd *c)
{
  Command cmd(c); // Create wrapper object
  Argument arg = cmd.getArgument(0);
  String argVal = arg.getValue();

  if (argVal.equals(F("hk")) || argVal.equals(""))
  {
    Serial.print(lat, 6);
    Serial.print(",");
    Serial.print(lon, 6);
    Serial.print(",");
    Serial.print(alt);
    Serial.print(",");
    Serial.print(sats);
    Serial.print(",");
    Serial.println(fix);
  }
}
