//LED pin definition
#define RED_LED A12 //GPS fix indicator
#define ORANGE_LED A14 //LoRa transmitt indicator
#define GREEN_LED 32 //power chanel emulator

//LoRa setup
#define LORA_NSS 34
#define LORA_RST 36
#define LORA_DIO0 2
#define LORA_FREQUENCY 867.0 //in MHz

//SD card
#define SD_SS 46

//NTC thermistor
#define C1 0.001125308852122
#define C2 0.000234711863267
#define C3 0.000000085663516
#define NTC_R 10E3 // 10k ohm thermistor
#define NTC A1 //ntc readings at A1 analog pin


/**********************************************
  Advanced sensor setup
 **********************************************/

//BME280 serial debug
//#define BME_debug

//BMP180 settings
// Ultra Low Power       OSS = 0, OSD =  5ms
// Standard              OSS = 1, OSD =  8ms
// High                  OSS = 2, OSD = 14ms
// Ultra High Resolution OSS = 3, OSD = 26ms
#define OSS 3                      // Set oversampling setting
#define OSD 26                     // with corresponding oversampling delay

//BMP180 serial debug
//#define BMP_debug

//MPU6050 settings
#define MPU_ACCEL_RESOLUTION 0x18 //0x00 - 2g; 0x08 - 4g; 0x10 - 8g; 0x18 - 16g 
#define MPU_GYRO_RESOLUTION 0 //0x00 - 250°/s; 0x08 - 500°/s; 0x10 - 1000°/s; 0x18 - 2000°/s

//QMC5883L settings
#define Mode_Standby    0b00000000
#define Mode_Continuous 0b00000001

#define ODR_10Hz        0b00000000
#define ODR_50Hz        0b00000100
#define ODR_100Hz       0b00001000
#define ODR_200Hz       0b00001100

#define RNG_2G          0b00000000
#define RNG_8G          0b00010000

#define OSR_512         0b00000000
#define OSR_256         0b01000000
#define OSR_128         0b10000000
#define OSR_64          0b11000000
