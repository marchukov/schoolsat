#include "config.h"
#include <Wire.h>
#include <RadioLib.h>
#include "base64.hpp"
#include <TinyGPS++.h>
#include<SPI.h>
#include <SD.h>
#include<SimpleCLI.h>

#define LEN 38 // full array length
//#define LEN 20 // pyload length, short

#define CRC_16_POLY 0x1021

unsigned long LoRa_previousMillis;
unsigned long serial_previousMillis;
unsigned int LoRa_interval = 5000; //radio send interval;
unsigned int serial_interval = 5000; //serial send interval
boolean LoRa_beacon_on = true;

/**********************************************
  Sensor selection variables
 **********************************************/
bool bmp, bme, hmc, qmc, mpu, lora, sd_card, solar = false;
byte bmp_address, bme280_address, qmc_address, hmc_address, mpu_address = 0x00;

//Gyroscopes, accelerometers and magnetometers data
int16_t mx, my, mz, mx_out, my_out, mz_out, mx_prev, my_prev, mz_prev;
int16_t ax, ay, az, ax_out, ay_out, az_out, ax_prev, ay_prev, az_prev;
int16_t gx, gy, gz, gx_out, gy_out, gz_out, gx_prev, gy_prev, gz_prev;

//bme280 data
int16_t temp;
uint32_t pressure;
uint32_t hum;

int16_t ntc_temp;

uint16_t batt_adc;

//GPS stuff
TinyGPSPlus gps;
float lon = 49.126;
float lat = 18.025;
uint16_t alt = 215;
uint8_t sats = 0;
uint8_t fix = 0;

//SDCard stuff
File dataFile;

//radio stuff
String sCommand;
RFM95 radio = new Module(LORA_NSS, LORA_DIO0, LORA_RST, 40);
SimpleCLI radioCli;
SimpleCLI serialCli;

uint8_t payload[LEN + 2];


/**********************************************
  Initial setup of satellite - runs once when powered on
 **********************************************/
void setup()
{
  initCmds();
  Wire.begin();
  LoRa_previousMillis = 0;
  serial_previousMillis = 0;

  Serial.begin(115200); //Serial COM port enable
  Serial3.begin(9600); // GPS serial port init
  sensors_ids();

  //init of all sensors ant transmitter
  if (mpu)
    init_MPU(mpu_address);

  if (qmc)
    init_QMC(qmc_address);

  if (bme)
    init_BME(bme280_address);

  if (bmp)
    init_BMP(bmp_address);

  if (hmc)
    Serial.println("Sensors not supported yet");

  if (!SD.begin(SD_SS))
    Serial.println("SD initialization failed!");
  else
  {
    Serial.println("SD initialization success!");
    sd_card = true;
  }
  init_LoRa();

  solar = solar_init();

  //initial csv

  if (sd_card && !SD.exists("data.csv"))
  {
    dataFile = SD.open("data.csv", FILE_WRITE);

    dataFile.print("uptime");
    dataFile.print(",");
    dataFile.print("gyro_x");
    dataFile.print(",");
    dataFile.print("gyro_y");
    dataFile.print(",");
    dataFile.print("gyro_z");
    dataFile.print(",");
    dataFile.print("acc_x");
    dataFile.print(",");
    dataFile.print("acc_y");
    dataFile.print(",");
    dataFile.print("acc_z");
    dataFile.print(",");
    dataFile.print("mag_x");
    dataFile.print(",");
    dataFile.print("mag_y");
    dataFile.print(",");
    dataFile.print("mag_z");
    dataFile.print(",");
    dataFile.print("bme280_press");
    dataFile.print(",");
    dataFile.print("bme280_temp");
    dataFile.print(",");
    dataFile.print("ntc_temp");
    dataFile.print(",");
    dataFile.print("batt_adc");
    dataFile.print(",");
    dataFile.print("lat");
    dataFile.print(",");
    dataFile.print("lon");
    dataFile.print(",");
    dataFile.print("alt");
    dataFile.print(",");
    dataFile.print("sats");
    dataFile.print(",");
    dataFile.println("fix");

    dataFile.close();
  }

  //set LEDs as output
  pinMode(RED_LED, OUTPUT);
  pinMode(ORANGE_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  //blink on startup
  for (int i = 0; i < 3; i++)
  {
    digitalWrite(RED_LED, HIGH);
    delay(100);
    digitalWrite(ORANGE_LED, HIGH);
    delay(100);
    digitalWrite(GREEN_LED, HIGH);
    delay(500);
    digitalWrite(RED_LED, LOW);
    delay(100);
    digitalWrite(ORANGE_LED, LOW);
    delay(100);
    digitalWrite(GREEN_LED, LOW);
    delay(500);
  }
  //disable LEDs on startup
  digitalWrite(RED_LED, LOW);
  digitalWrite(ORANGE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
}

void loop()
{
  receive_LoRa();
  
  while (Serial3.available())
  {
    char c = Serial3.read();
    gps.encode(c);
  }

  if (bmp && !bme)
    bmp_getPT(&pressure, &temp);
  else if (bme)
    bme_getPT(&pressure, &temp);

  getAccel(&ax, &ay, &az);
  getGyro(&gx, &gy, &gz);
  getMag(&mx, &my, &mz);

  getGPS(&lat, &lon, &alt, &sats, &fix);

  ntc_temp = ntc_readTemperature();

  batt_adc = analogRead(A0);

  fillArray();

  if (solar) {
    solar_measure();
  }
  
  unsigned long currentMillis = millis();
  if (currentMillis - serial_previousMillis >= serial_interval)
  {
    serial_previousMillis = currentMillis;

    Serial.print((long)(millis() / 1000));
    Serial.print(",");
    Serial.print(gx);
    Serial.print(",");
    Serial.print(gy);
    Serial.print(",");
    Serial.print(gz);
    Serial.print(",");
    Serial.print(ax);
    Serial.print(",");
    Serial.print(ay);
    Serial.print(",");
    Serial.print(az);
    Serial.print(",");
    Serial.print(mx);
    Serial.print(",");
    Serial.print(my);
    Serial.print(",");
    Serial.print(mz);
    Serial.print(",");
    Serial.print(pressure);
    Serial.print(",");
    Serial.print(temp);
    Serial.print(",");
    Serial.print(ntc_temp);
    Serial.print(",");
    Serial.print(batt_adc);
    Serial.print(",");
    Serial.print(lat, 6);
    Serial.print(",");
    Serial.print(lon, 6);
    Serial.print(",");
    Serial.print(alt);
    Serial.print(",");
    Serial.print(sats);
    Serial.print(",");
    Serial.println(fix);
    Serial.println("======");
    
    if (solar) {
      solar_print();
    }
    
    if (sd_card)
    {
      dataFile = SD.open("data.csv", FILE_WRITE);

      dataFile.print((long)(millis() / 1000));
      dataFile.print(",");
      dataFile.print(gx);
      dataFile.print(",");
      dataFile.print(gy);
      dataFile.print(",");
      dataFile.print(gz);
      dataFile.print(",");
      dataFile.print(ax);
      dataFile.print(",");
      dataFile.print(ay);
      dataFile.print(",");
      dataFile.print(az);
      dataFile.print(",");
      dataFile.print(mx);
      dataFile.print(",");
      dataFile.print(my);
      dataFile.print(",");
      dataFile.print(mz);
      dataFile.print(",");
      dataFile.print(pressure);
      dataFile.print(",");
      dataFile.print(temp);
      dataFile.print(",");
      dataFile.print(ntc_temp);
      dataFile.print(",");
      dataFile.print(batt_adc);
      dataFile.print(",");
      dataFile.print(lat, 6);
      dataFile.print(",");
      dataFile.print(lon, 6);
      dataFile.print(",");
      dataFile.print(alt);
      dataFile.print(",");
      dataFile.print(sats);
      dataFile.print(",");
      dataFile.println(fix);

      dataFile.close();
    }
  }

  currentMillis = millis();
  if (currentMillis - LoRa_previousMillis >= LoRa_interval && lora && LoRa_beacon_on)
  {
    LoRa_previousMillis = currentMillis;

    uint16_t i_crc = crc16(payload, sizeof(payload) - 2);
    memcpy(payload + LEN, &i_crc, 2);
    send_LoRa(payload, sizeof(payload));
  }
}

void sensors_ids()
{
  //availiable addresses
  byte error, address, id;

  for (address = 1; address < 127; address++)
  {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C sensor at 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);

      //BMP/BME identification
      if (address == 0x76 || address == 0x77)
      {
        Wire.beginTransmission(address);
        Wire.write(0xD0);
        Wire.endTransmission();
        Wire.requestFrom(address, 1);
        if (Wire.available() >= 1)
        {
          id = Wire.read();
        }

        if (id == 0x55)
        {
          bmp = true;
          bmp_address = address;
        }
        else if (id == 0x58)
        {
          bmp = true;
          bmp_address = address;
        }
        else if (id == 0x60)
        {
          bme = true;
          bme280_address = address;
        }
        Serial.print("Id of sensor ");
        Serial.println(id, HEX);
      }

      //MPU6050 identification
      else if (address == 0x68 || address == 0x69)
      {
        Wire.beginTransmission(address);
        Wire.write(0x75);
        Wire.endTransmission();
        Wire.requestFrom(address, 1);
        if (Wire.available() >= 1)
        {
          id = Wire.read();
        }
        Serial.print("Id of sensor ");
        if (id == 0x68)
        {
          mpu = true;
          mpu_address = address;
        }
        else
          Serial.println("Unknown type of sensor");
        Serial.println(id, HEX);
      }

      //HMC/QMC identification
      else
      {
        byte read_error;
        Wire.beginTransmission(address);
        Wire.write(0x0D);
        read_error = Wire.endTransmission();
        if (read_error == 0)
        {
          Wire.requestFrom(address, 1);
          if (Wire.available() >= 1)
          {
            id = Wire.read();
          }
          if (id == 0xFF)
          {
            qmc = true;
            qmc_address = address;
          }
          Serial.print("Id of sensor ");
          Serial.println(id, HEX);
        }
        else
        {
          Wire.beginTransmission(address);
          Wire.write(0x0A);
          read_error = Wire.endTransmission();
          if (read_error == 0)
          {
            if (Wire.available() >= 1)
            {
              id = Wire.read();
            }
            if (id == 0x48)
            {
              hmc = true;
              hmc_address = address;
            }
            Serial.print("Id of sensor ");
            Serial.println(id, HEX);
          }
          else
            Serial.println("Unknown type of sensor");
        }
      }
    }
  }
}

unsigned short crc16(char *data_p, unsigned short length)
{
  unsigned char i;
  unsigned int data;
  unsigned int crc = 0xffff;

  if (length == 0)
    return (~crc);

  do
  {
    for (i = 0, data = (unsigned int)0xff & *data_p++;
         i < 8;
         i++, data >>= 1)
    {
      if ((crc & 0x0001) ^ (data & 0x0001))
        crc = (crc >> 1) ^ CRC_16_POLY;
      else
        crc >>= 1;
    }
  } while (--length);

  crc = ~crc;
  data = crc;
  crc = (crc << 8) | (data >> 8 & 0xff);

  return (crc);
}


void serialEvent()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    if (inChar != '\r')
      sCommand += inChar;
    if (inChar == '\n')
    {
      Serial.print("# ");
      Serial.println(sCommand);
      serialCli.parse(sCommand);
      sCommand = "";
    }
  }
}

// basic full array
void fillArray()
{
  memcpy(payload + 0, &gx, 2);
  memcpy(payload + 2, &gy, 2);
  memcpy(payload + 4, &gz, 2);

  memcpy(payload + 6, &ax, 2);
  memcpy(payload + 8, &ay, 2);
  memcpy(payload + 10, &az, 2);

  memcpy(payload + 12, &mx, 2);
  memcpy(payload + 14, &my, 2);
  memcpy(payload + 16, &mz, 2);

  memcpy(payload + 18, &pressure, 4);
  memcpy(payload + 22, &temp, 2);
  
  memcpy(payload + 24, &batt_adc, 2);
  
  memcpy(payload + 26, &lat, 4);
  memcpy(payload + 30, &lon, 4);
  memcpy(payload + 34, &alt, 2);
  memcpy(payload + 36, &sats, 1);
  memcpy(payload + 37, &fix, 1);
}

/*
// shorter array without 9DOF data
void fillArray()
{
  memcpy(payload + 0, &pressure, 4);
  memcpy(payload + 4, &batt_adc, 2);
  memcpy(payload + 6, &ntc_temp, 2);
  memcpy(payload + 8, &lat, 4);
  memcpy(payload + 12, &lon, 4);
  memcpy(payload + 16, &alt, 2);
  memcpy(payload + 18, &sats, 1);
  memcpy(payload + 19, &fix, 1);
}
*/
