#include "config.h"

int ntc_readTemperature()
{
  int iNtc_analog;
  float logR2, R2, T_K, T_degC;
  
  iNtc_analog = analogRead(NTC);
  R2 = NTC_R * ((1023.0) / (float)iNtc_analog - 1.0);
  logR2 = log(R2);
  T_K = (1.0 / (C1 + C2 * logR2 + C3 * logR2 * logR2 * logR2));
  T_degC = T_K -273.15;

  return (int)(T_degC*100); //return temperature in 0.01*C
 }
