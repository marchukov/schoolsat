#include "config.h"

byte _qmc_address;

void setMode_QMC(uint16_t mode, uint16_t odr, uint16_t rng, uint16_t osr)
{
  write8(_qmc_address, 0x09, mode | odr | rng | osr);
}


void softReset_QMC()
{
  write8(_qmc_address, 0x0A, 0x80);
}


void init_QMC(byte address)
{
  _qmc_address = address;
  Serial.println("QMC5883L init started...");
  write8(_qmc_address, 0x0B, 0x01);
  //Define Set/Reset period
  setMode_QMC(Mode_Continuous, ODR_50Hz, RNG_2G, OSR_512);
  /*
    Define
    OSR = 512
    Full Scale Range = 8G(Gauss)
    ODR = 200HZ
    set continuous measurement mode
  */
}

void getMag(uint16_t* x, uint16_t* y, uint16_t* z)
{
  *x = readS16_LE(_qmc_address, 0x00);
  *y = readS16_LE(_qmc_address, 0x02);
  *z = readS16_LE(_qmc_address, 0x04);
}
