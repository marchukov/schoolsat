#include <Wire.h>
#include <Adafruit_INA219.h>

Adafruit_INA219 ina219_Yn(0x40);
Adafruit_INA219 ina219_Yp(0x41);
Adafruit_INA219 ina219_Xp(0x44);
Adafruit_INA219 ina219_Xn(0x45);

bool solar_init() {
    // Initialize the INA219.
    // By default the initialization will use the largest range (32V, 2A).  However
    // you can call a setCalibration function to change this range (see comments).
    if (!ina219_Xp.begin())
    {
      Serial.println("Failed to find INA219 chip Xp");
      while (1)
      {
        delay(10);
      }
    }
    if (!ina219_Xn.begin())
    {
      Serial.println("Failed to find INA219 chip Xn");
      while (1)
      {
        delay(10);
      }
    }
    if (!ina219_Yp.begin())
    {
      Serial.println("Failed to find INA219 chip Yn");
      while (1)
      {
        delay(10);
      }
    }
    if (!ina219_Yn.begin())
    {
      Serial.println("Failed to find INA219 chip Yn");
      while (1)
      {
        delay(10);
      }
    }
    
    // To use a slightly lower 32V, 1A range (higher precision on amps):
    // ina219.setCalibration_32V_1A();
    // Or to use a lower 16V, 400mA range (higher precision on volts and amps):
    // ina219.setCalibration_16V_400mA();
  
    Serial.println("Measuring voltage and current with INA219 ...");

    return true;
}

float shuntvoltage_Xp, shuntvoltage_Xn, shuntvoltage_Yp, shuntvoltage_Yn = 0;
float busvoltage_Xp, busvoltage_Xn, busvoltage_Yp, busvoltage_Yn = 0;
float current_mA_Xp, current_mA_Xn, current_mA_Yp, current_mA_Yn = 0;
float loadvoltage_Xp, loadvoltage_Xn, loadvoltage_Yp, loadvoltage_Yn = 0;
float power_mW_Xp, power_mW_Xn, power_mW_Yp, power_mW_Yn = 0;

void solar_measure() {
    shuntvoltage_Xp = ina219_Xp.getShuntVoltage_mV();
    busvoltage_Xp = ina219_Xp.getBusVoltage_V();
    current_mA_Xp = ina219_Xp.getCurrent_mA();
    power_mW_Xp = ina219_Xp.getPower_mW();
    loadvoltage_Xp = busvoltage_Xp + (shuntvoltage_Xp / 1000);
  
    shuntvoltage_Xn = ina219_Xn.getShuntVoltage_mV();
    busvoltage_Xn = ina219_Xn.getBusVoltage_V();
    current_mA_Xn = ina219_Xn.getCurrent_mA();
    power_mW_Xn = ina219_Xn.getPower_mW();
    loadvoltage_Xn = busvoltage_Xn + (shuntvoltage_Xn / 1000);
    
    shuntvoltage_Yp = ina219_Yp.getShuntVoltage_mV();
    busvoltage_Yp = ina219_Yp.getBusVoltage_V();
    current_mA_Yp = ina219_Yp.getCurrent_mA();
    power_mW_Yp = ina219_Yp.getPower_mW();
    loadvoltage_Yp = busvoltage_Yp + (shuntvoltage_Yp / 1000);
  
    shuntvoltage_Yn = ina219_Yn.getShuntVoltage_mV();
    busvoltage_Yn = ina219_Yn.getBusVoltage_V();
    current_mA_Yn = ina219_Yn.getCurrent_mA();
    power_mW_Yn = ina219_Yn.getPower_mW();
    loadvoltage_Yn = busvoltage_Yn + (shuntvoltage_Yn / 1000);
}

void solar_print() {
    Serial.print("Xp  B_V: "); Serial.print(busvoltage_Xp); Serial.print(" V");
    Serial.print(" S_V: "); Serial.print(shuntvoltage_Xp); Serial.print( " mV");
    Serial.print(" L_V: "); Serial.print(loadvoltage_Xp); Serial.print(" V");
    Serial.print(" C:   "); Serial.print(current_mA_Xp); Serial.print(" mA");
    Serial.print(" P:   "); Serial.print(power_mW_Xp); Serial.println(" mW");
    
    Serial.print("Xn  B_V: "); Serial.print(busvoltage_Xn); Serial.print(" V");
    Serial.print(" S_V: "); Serial.print(shuntvoltage_Xn); Serial.print( " mV");
    Serial.print(" L_V: "); Serial.print(loadvoltage_Xn); Serial.print(" V");
    Serial.print(" C:   "); Serial.print(current_mA_Xn); Serial.print(" mA");
    Serial.print(" P:   "); Serial.print(power_mW_Xn); Serial.println(" mW");
    
    Serial.print("Yp  B_V: "); Serial.print(busvoltage_Yp); Serial.print(" V");
    Serial.print(" S_V: "); Serial.print(shuntvoltage_Yp); Serial.print( " mV");
    Serial.print(" L_V: "); Serial.print(loadvoltage_Yp); Serial.print(" V");
    Serial.print(" C:   "); Serial.print(current_mA_Yp); Serial.print(" mA");
    Serial.print(" P:   "); Serial.print(power_mW_Yp); Serial.println(" mW");
    
    Serial.print("Yn  B_V: "); Serial.print(busvoltage_Yn); Serial.print(" V");
    Serial.print(" S_V: "); Serial.print(shuntvoltage_Yn); Serial.print( " mV");
    Serial.print(" L_V: "); Serial.print(loadvoltage_Yn); Serial.print(" V");
    Serial.print(" C:   "); Serial.print(current_mA_Yn); Serial.print(" mA");
    Serial.print(" P:   "); Serial.print(power_mW_Yn); Serial.println(" mW");
    Serial.println("|------------------------------------------------------------------------|");
}
