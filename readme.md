# Satellite
This repository section is used to store hardware and firmware for the Schoolsat satellite platform design.

## Folder structure
The project is divided into two main folders **Receiver Dongle** and **Satellite**. \
These then follow the following structure:

**CAD** - holds design files and 3D printing files for the satellite structure

**HW** - holds PCB design of the various modules of the Schoolsat satellite

**FW** - holds firmware (Arduino or other) for the Schoolsat satellite platform